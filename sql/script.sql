--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.2
-- Dumped by pg_dump version 9.5.2

-- Started on 2016-09-06 08:03:02

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 1 (class 3079 OID 12355)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2116 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 182 (class 1259 OID 52260)
-- Name: adesivo; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE adesivo (
    id integer NOT NULL,
    descricao character varying,
    preco double precision
);


ALTER TABLE adesivo OWNER TO postgres;

--
-- TOC entry 181 (class 1259 OID 52258)
-- Name: adesivo_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE adesivo_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE adesivo_id_seq OWNER TO postgres;

--
-- TOC entry 2117 (class 0 OID 0)
-- Dependencies: 181
-- Name: adesivo_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE adesivo_id_seq OWNED BY adesivo.id;


--
-- TOC entry 183 (class 1259 OID 52269)
-- Name: usuario; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE usuario (
    email character varying(50) NOT NULL,
    senha character varying(50),
    nome character varying(50),
    datanasc date,
    sexo character(1),
    tipo character varying
);


ALTER TABLE usuario OWNER TO postgres;

--
-- TOC entry 1987 (class 2604 OID 52263)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY adesivo ALTER COLUMN id SET DEFAULT nextval('adesivo_id_seq'::regclass);


--
-- TOC entry 2107 (class 0 OID 52260)
-- Dependencies: 182
-- Data for Name: adesivo; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO adesivo VALUES (1, 'primeiro', 10);
INSERT INTO adesivo VALUES (2, 'segundo', 20);
INSERT INTO adesivo VALUES (3, 'terceiro', 30);


--
-- TOC entry 2118 (class 0 OID 0)
-- Dependencies: 181
-- Name: adesivo_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('adesivo_id_seq', 3, true);


--
-- TOC entry 2108 (class 0 OID 52269)
-- Dependencies: 183
-- Data for Name: usuario; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO usuario VALUES ('email', '123', 'nome', '2016-09-04', 'M', 'CLIENTE');
INSERT INTO usuario VALUES ('email2', '123', '123', '2016-09-04', 'M', 'CLIENTE');
INSERT INTO usuario VALUES ('email3', 'senha', 'nome', '2016-09-05', 'F', 'CLIENTE');
INSERT INTO usuario VALUES ('email5', 'senha', '123', '2016-09-05', 'M', 'CLIENTE');
INSERT INTO usuario VALUES ('admin', 'admin', 'admin', '2016-09-05', 'K', 'ADMINISTRADOR');
INSERT INTO usuario VALUES ('email10', '123', '123', '2016-09-05', 'M', 'CLIENTE');
INSERT INTO usuario VALUES ('email11', '123', '123', '2016-09-05', 'M', 'CLIENTE');


--
-- TOC entry 1989 (class 2606 OID 52268)
-- Name: adesivo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY adesivo
    ADD CONSTRAINT adesivo_pkey PRIMARY KEY (id);


--
-- TOC entry 1991 (class 2606 OID 52276)
-- Name: usuario_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY usuario
    ADD CONSTRAINT usuario_pkey PRIMARY KEY (email);


--
-- TOC entry 2115 (class 0 OID 0)
-- Dependencies: 6
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2016-09-06 08:03:03

--
-- PostgreSQL database dump complete
--

