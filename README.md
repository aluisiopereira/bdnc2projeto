### 2º PROJETO DE BDNC, PERSISTÊNCIA POLIGLOTA
####(Loja de adesivos)
#### Histórico de revisão
Data          |   Versão  |                        Descrição                             |  Autor(es)
------------- | --------- | ------------------------------------------------------------ | --------------------------------
06/09/2016    |   1.0.0   | Especificação do uso da persistencia poliglota no projeto   | [Aluísio Pereira](https://github.com/AluisioPereira) / [José Marcondes](https://bitbucket.org/MarcondesADS/) 

## 1 CONSIDERAÇÕES INICIAIS
Para fins de documentação do sistema de e-commerce, loja de venda de adesivos temos este relato que abordará como se deu a construção deste sistema mediante o uso da persistência poliglota, de forma apresentar cada abordagem determinado o porquê da utilização de cada banco de dados aqui especificado para cada situação, partindo de ponto como suas principais caraterísticas e peculiaridades para a tomada de decisão de uso neste sistema.

## 2 USO DOS BANCOS
### 2.1 Redis

### 2.2 MongoDB

### 2.3 PostgreSQL

### 2.4 Neo4j

## 3 CONSIDERAÇÕES FINAIS
Este projeto realizado para a disciplina de BDNC (Banco de Dados Não Convencionais) 2016.1, do curso de ADS (Análise e Desenvolvimento de Sistemas) do IFPB (Instituto Federal de Educação, Ciências e Tecnologia da Paraíba), Campus Cajazeiras, tendo como construção de um sistema de loja virtual de venda de adesivos constumisados para programação, mediante isso este sistema proporcionará uma melhor interação entre os clientes e a loja.   
  
