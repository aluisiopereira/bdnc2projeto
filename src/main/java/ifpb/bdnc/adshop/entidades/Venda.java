package ifpb.bdnc.adshop.entidades;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 *
 * @author José Marcondes do Nascimento Junior
 */
public class Venda implements Serializable{
    
    private String id;
    private List<ItemVenda> itens;
    private Usuario cliente;
    private LocalDate data;

    public Venda(CarrinhoCompra carrinho) {
        this.id = UUID.randomUUID().toString();
        itens = carrinho.getItens();
        cliente = carrinho.getDono();
        data = LocalDate.now();
    }
    

    public Venda(Usuario cliente) {
        this.itens = new ArrayList<>();
        this.cliente = cliente;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<ItemVenda> getItens() {
        return itens;
    }

    public void setItens(List<ItemVenda> itens) {
        this.itens = itens;
    }
    
    public void addItem(Adesivo adesivo, int quant){
        this.itens.add(new ItemVenda(adesivo, quant));
    }

    public Usuario getCliente() {
        return cliente;
    }

    public void setCliente(Usuario cliente) {
        this.cliente = cliente;
    }

    public LocalDate getData() {
        return data;
    }

    public void setData(LocalDate data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "Venda{" + "id=" + id + ", itens=" + itens + ", cliente=" + cliente + ", data=" + data + '}';
    }
        
    
}
