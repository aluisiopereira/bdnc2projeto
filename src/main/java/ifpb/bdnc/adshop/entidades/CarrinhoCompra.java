package ifpb.bdnc.adshop.entidades;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author José Marcondes do Nascimento Junior
 */
public class CarrinhoCompra implements Serializable{
    
    private String id;
    private List<ItemVenda> itens;
    private Usuario dono;

    public CarrinhoCompra() {
    }


    public CarrinhoCompra(String id,Usuario dono) {
        this.id = id;
        this.itens = new ArrayList<>();
        this.dono = dono;
    }

    public String getId() {
        return id;
    }
    
    public void addItem(Adesivo adesivo, int quant){
        this.itens.add(new ItemVenda(adesivo, quant));
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<ItemVenda> getItens() {
        return itens;
    }

    public void setItens(List<ItemVenda> itens) {
        this.itens = itens;
    }

    public Usuario getDono() {
        return dono;
    }

    public void setDono(Usuario dono) {
        this.dono = dono;
    }
    
    
    
}
