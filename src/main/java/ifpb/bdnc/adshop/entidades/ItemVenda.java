package ifpb.bdnc.adshop.entidades;

import java.io.Serializable;

/**
 *
 * @author José Marcondes do Nascimento Junior
 */
public class ItemVenda implements Serializable{
    
    private Adesivo item;
    private int quantidade;

    public ItemVenda() {
    }
    

    public ItemVenda(Adesivo item, int quantidade) {
        this.item = item;
        this.quantidade = quantidade;
    }

    public Adesivo getItem() {
        return item;
    }

    public void setItem(Adesivo item) {
        this.item = item;
    }


    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }
    
    
    
}
