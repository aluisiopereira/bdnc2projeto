package ifpb.bdnc.adshop.entidades;

/**
 *
 * @author José Marcondes do Nascimento Junior
 */
public enum TipoUsuario {
    CLIENTE, ADMINISTRADOR
}
