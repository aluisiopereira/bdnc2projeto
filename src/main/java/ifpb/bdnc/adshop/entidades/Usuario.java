package ifpb.bdnc.adshop.entidades;

import java.io.Serializable;
import java.time.LocalDate;

/**
 *
 * @author José Marcondes do Nascimento Junior
 */
public class Usuario implements Serializable{
    
    private String email;
    private String senha;
    private String nome;
    private LocalDate dataNascimento;
    private String sexo;
    private TipoUsuario tipo;

    public Usuario(String email) {
        this.email = email;
    }

    public Usuario(String email, String senha, String nome, LocalDate dataNascimento, String sexo, TipoUsuario tipo) {
        this.email = email;
        this.senha = senha;
        this.nome = nome;
        this.dataNascimento = dataNascimento;
        this.sexo = sexo;
        this.tipo = tipo;
    }

    public Usuario() {
    }
    

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public LocalDate getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(LocalDate dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public TipoUsuario getTipo() {
        return tipo;
    }

    public void setTipo(TipoUsuario tipo) {
        this.tipo = tipo;
    }
    
    
    
}
