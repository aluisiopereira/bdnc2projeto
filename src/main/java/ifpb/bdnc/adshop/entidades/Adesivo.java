package ifpb.bdnc.adshop.entidades;

import java.io.Serializable;

/**
 *
 * @author José Marcondes do Nascimento Junior
 */
public class Adesivo implements Serializable{
    
    private Integer id;
    private String descricao;
    private double preco;

    public Adesivo() {
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public double getPreco() {
        return preco;
    }

    public void setPreco(double preco) {
        this.preco = preco;
    }
    
    
        
}
