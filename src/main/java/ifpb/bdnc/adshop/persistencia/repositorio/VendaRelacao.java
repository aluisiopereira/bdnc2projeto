package ifpb.bdnc.adshop.persistencia.repositorio;

import ifpb.bdnc.adshop.entidades.ItemVenda;
import ifpb.bdnc.adshop.entidades.Usuario;
import ifpb.bdnc.adshop.entidades.Venda;
import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Label;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.ResourceIterator;
import org.neo4j.graphdb.Result;
import org.neo4j.graphdb.Transaction;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;
import org.neo4j.kernel.impl.factory.GraphDatabaseFacade;

/**
 *
 * @author José Marcondes do Nascimento Junior
 */
@Named
@ApplicationScoped
public class VendaRelacao implements Serializable{

    private GraphDatabaseService graph;
    
    @PreDestroy
    public void destroy(){
        graph.shutdown();
    }
    
    @PostConstruct
    public void contruct(){
         try {
            Configuration configs = new PropertiesConfiguration(this.getClass().getResource("/database/neo4j.properties"));
            File file = new File(configs.getString("filePath"));
            graph = new GraphDatabaseFactory().newEmbeddedDatabase(file);
        } catch (ConfigurationException ex) {
            Logger.getLogger(VendaRelacao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public VendaRelacao() {
        
    }

    public void criarRelacao(Venda venda) {
        try (Transaction tx = graph.beginTx()) {

            Node usuario = graph.findNode(Label.label("Cliente"), "email", venda.getCliente().getEmail());
            if (usuario == null) {
                usuario = graph.createNode(Label.label("Cliente"));
                usuario.setProperty("email", venda.getCliente().getEmail());
            }

            List<Node> produtos = new ArrayList<>();

            for (ItemVenda item : venda.getItens()) {
                Node produto = graph.findNode(Label.label("Produto"), "id", item.getItem().getId());
                if (produto == null) {
                    produto = graph.createNode(Label.label("Produto"));
                    produto.setProperty("id", item.getItem().getId());
                }
                produtos.add(produto);
            }

            for (Node n : produtos) {
                usuario.createRelationshipTo(n, TipoRelacao.COMPROU);
            }

            tx.success();

        }

    }

    public List<Integer> recomendados(Usuario p) {

        List<Integer> recomendados = new ArrayList<>();

        try (Transaction tx = graph.beginTx()) {

            String query = "MATCH (u:Cliente) - [:COMPROU] -> (:Produto) <-[:COMPROU]-(u2:Cliente)-[:COMPROU]->"
                    + "(p:Produto) WHERE u.email= \"" + p.getEmail() + "\" and "
                    + "u2.email <> \""+p.getEmail()+"\" RETURN  distinct p.id";
            Result result = graph.execute(query);

            ResourceIterator<Integer> resourceIterator = result.columnAs("p.id");
            while (resourceIterator.hasNext()) {
                recomendados.add(resourceIterator.next());
            }
            
            tx.success();

        }
        
        return recomendados;

    }


}
