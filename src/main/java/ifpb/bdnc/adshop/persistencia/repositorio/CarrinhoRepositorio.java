package ifpb.bdnc.adshop.persistencia.repositorio;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;
import ifpb.bdnc.adshop.entidades.CarrinhoCompra;
import java.util.List;
import java.util.Map;
import javax.annotation.PreDestroy;
import javax.inject.Inject;
import redis.clients.jedis.Jedis;

/**
 *
 * @author José Marcondes do Nascimento Junior
 */
public class CarrinhoRepositorio implements Repositorio<CarrinhoCompra, String> {

    @Inject
    private Jedis jedis;
    
    @PreDestroy
    public void destroy(){
        jedis.close();
    }

    @Override
    public CarrinhoCompra criar(CarrinhoCompra obj) {
        Kryo kryo = new Kryo();
        Output out = new Output(new byte[512]);
        kryo.writeObject(out, obj);
        out.flush();
        byte[] bytes = out.toBytes();
        out.close();
        jedis.set(obj.getId(), new String(bytes));
        return obj;
    }

    @Override
    public CarrinhoCompra encontrar(String id) {

        Kryo kryo = new Kryo();
        String value = jedis.get(id);
        if (value == null) {
            return null;
        }
        byte[] bytes = value.getBytes();
        Input in = new Input(bytes);
        
        CarrinhoCompra car = kryo.readObject(in, CarrinhoCompra.class);
        in.close();
        return car;

    }

    @Override
    public List<CarrinhoCompra> encontrar(Map<String, String> campos) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<CarrinhoCompra> encontrar() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public CarrinhoCompra atualizar(CarrinhoCompra obj) {
        if (encontrar(obj.getId()) != null) {
            criar(obj);
            return obj;
        } else {
            return null;
        }
    }

    @Override
    public CarrinhoCompra deletar(String id) {
        CarrinhoCompra obj = encontrar(id);
        if (obj != null) {
            jedis.del(id.getBytes());
        }
        return obj;
    }

    public Jedis getJedis() {
        return jedis;
    }

    public void setJedis(Jedis jedis) {
        this.jedis = jedis;
    }

}
