package ifpb.bdnc.adshop.persistencia.repositorio;

import com.mongodb.MongoClient;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoDatabase;
import ifpb.bdnc.adshop.entidades.Adesivo;
import ifpb.bdnc.adshop.entidades.ItemVenda;
import ifpb.bdnc.adshop.entidades.Usuario;
import ifpb.bdnc.adshop.entidades.Venda;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PreDestroy;
import javax.inject.Inject;
import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.bson.Document;

/**
 *
 * @author José Marcondes do Nascimento Junior
 */
public class VendaRepositorio implements Repositorio<Venda, String>{
    
    @Inject
    private MongoClient mongoClient;
    @Inject
    private AdesivoRepositorio adesivoRepositorio;
    @Inject
    private UsuarioRepositorio usuarioRepositorio;
    
    @PreDestroy
    private void destroy(){
        mongoClient.close();
    }


    @Override
    public Venda criar(Venda obj) {
        Document doc = new Document("_id",obj.getId())
                .append("clienteEmail", obj.getCliente().getEmail())
                .append("data", obj.getData().toString());
        List<Document> list = new ArrayList<>();
        for(ItemVenda item : obj.getItens()){
            Document docItem = new Document("itemId", item.getItem().getId())
                    .append("itemQuant", item.getQuantidade());
            list.add(docItem);
        }
        doc.append("itens", list);
        MongoDatabase mongoDatabase = getDatabase();
        mongoDatabase.getCollection("Vendas").insertOne(doc);
        return obj;
    }

    @Override
    public Venda encontrar(String id) {
        MongoDatabase mongoDatabase = getDatabase();
        FindIterable<Document> findIterable = mongoDatabase.getCollection("Vendas").find(new Document("_id", id));
        Document doc = findIterable.first();
        String cliente = doc.getString("clienteEmail");
        String data = doc.getString("data");
        List<Document> itens = doc.get("itens", List.class);
        List<ItemVenda> itensVenda = new ArrayList<>();
        for (Document itemDoc : itens) {
            Integer itemId = itemDoc.getInteger("itemId");
            Integer itemQuant = itemDoc.get("itemQuant", Integer.class);
            ItemVenda itemVenda = new ItemVenda();
            
            Adesivo adesivo = adesivoRepositorio.encontrar(itemId);
            
            itemVenda.setItem(adesivo);
            itemVenda.setQuantidade(itemQuant);
            
            itensVenda.add(itemVenda);
        }
        
        Usuario usr = usuarioRepositorio.encontrar(id);
        
        LocalDate localdate = LocalDate.parse(data);
        
        Venda venda = new Venda(usr);
        venda.setItens(itensVenda);
        venda.setId(id);
        venda.setData(localdate);
        return venda;
    }

    private MongoDatabase getDatabase() {
        Configuration configs = null;
        try {
            configs = new PropertiesConfiguration(this.getClass().getResource("/database/dbmongo.properties"));
        } catch (ConfigurationException ex) {
            Logger.getLogger(VendaRepositorio.class.getName()).log(Level.SEVERE, null, ex);
        }
        MongoDatabase mongoDatabase = mongoClient.getDatabase(configs.getString("dataBase"));
        return mongoDatabase;
    }

    @Override
    public List<Venda> encontrar(Map<String, String> campos) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Venda> encontrar() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Venda atualizar(Venda obj) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Venda deletar(String id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public MongoClient getMongoDatabase() {
        return mongoClient;
    }

    public void setMongoDatabase(MongoClient mongoClient) {
        this.mongoClient = mongoClient;
    }

    public AdesivoRepositorio getAdesivoRepositorio() {
        return adesivoRepositorio;
    }

    public void setAdesivoRepositorio(AdesivoRepositorio adesivoRepositorio) {
        this.adesivoRepositorio = adesivoRepositorio;
    }

    public UsuarioRepositorio getUsuarioRepositorio() {
        return usuarioRepositorio;
    }

    public void setUsuarioRepositorio(UsuarioRepositorio usuarioRepositorio) {
        this.usuarioRepositorio = usuarioRepositorio;
    }
    
    
    
}
