package ifpb.bdnc.adshop.persistencia.repositorio;

import org.neo4j.graphdb.RelationshipType;

/**
 *
 * @author José Marcondes do Nascimento Junior
 */
public enum TipoRelacao implements RelationshipType{

    COMPROU;
    
}
