package ifpb.bdnc.adshop.persistencia.repositorio.connections;

import javax.enterprise.inject.Produces;
import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import redis.clients.jedis.Jedis;

/**
 *
 * @author José Marcondes do Nascimento Junior
 */
public class RedisConnectionProducer {
 
    @Produces
    public Jedis produce() throws ConfigurationException{
        Configuration configs = new PropertiesConfiguration(this.getClass().getResource("/database/dbredis.properties"));
        Jedis jedis = new Jedis(configs.getString("host"), configs.getInt("port"));
        return jedis;
    }
    
}
