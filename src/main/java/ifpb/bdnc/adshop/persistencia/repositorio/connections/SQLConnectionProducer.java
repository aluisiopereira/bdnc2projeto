package ifpb.bdnc.adshop.persistencia.repositorio.connections;

import java.io.IOException;
import java.util.Properties;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import javax.sql.DataSource;
import org.apache.commons.dbcp2.DriverManagerConnectionFactory;
import org.apache.commons.dbcp2.PoolableConnection;
import org.apache.commons.dbcp2.PoolableConnectionFactory;
import org.apache.commons.dbcp2.PoolingDataSource;
import org.apache.commons.pool2.ObjectPool;
import org.apache.commons.pool2.impl.GenericObjectPool;

/**
 *
 * @author José Marcondes do Nascimento Junior
 */
public class SQLConnectionProducer {
    
    @Produces
    @ApplicationScoped
    public DataSource getDataSource() throws ClassNotFoundException, IOException {
        Properties prop = new Properties();
        prop.load(this.getClass().getResourceAsStream("/database/dbsql.properties"));
        Class.forName(prop.getProperty("driver"));  
        DriverManagerConnectionFactory connectionFactory = new DriverManagerConnectionFactory(prop.getProperty("url"),prop);
        PoolableConnectionFactory poolableCFac = new PoolableConnectionFactory(connectionFactory, null);
        ObjectPool<PoolableConnection> objPool = new GenericObjectPool<>(poolableCFac);
        poolableCFac.setPool(objPool);
        PoolingDataSource<PoolableConnection> poolDS = new PoolingDataSource<>(objPool);
        return poolDS;
    } 
    
}
