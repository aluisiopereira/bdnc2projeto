package ifpb.bdnc.adshop.persistencia.repositorio;

import ifpb.bdnc.adshop.entidades.Adesivo;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.inject.Inject;
import javax.sql.DataSource;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;

/**
 *
 * @author José Marcondes do Nascimento Junior
 */
public class AdesivoRepositorio implements Repositorio<Adesivo, Integer> {

    @Inject
    private DataSource ds;

    @Override
    public Adesivo criar(Adesivo obj) {
        try {
            QueryRunner qr = new QueryRunner(ds);
            String sql = "INSERT INTO adesivo(id, descricao, preco) VALUES (?, ?, ?);";
            ResultSetHandler<Adesivo> rsh = (ResultSet rs) -> {
                if (rs.next()) {
                    obj.setId(rs.getInt("id"));
                    return obj;
                } else {
                    return null;
                }
            };
            return qr.insert(sql, rsh);
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioRepositorio.class.getName()).log(Level.SEVERE, null, ex);
        }
        return obj;
    }

    @Override
    public Adesivo encontrar(Integer id) {
        try {
            QueryRunner qr = new QueryRunner(ds);
            String sql = "SELECT * FROM ADESIVO WHERE id = ?";
            return qr.query(sql, new BeanHandler<Adesivo>(Adesivo.class), id);
        } catch (SQLException ex) {
            Logger.getLogger(AdesivoRepositorio.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public List<Adesivo> encontrar(Map<String, String> campos) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Adesivo> encontrar() {
        try {
            QueryRunner qr = new QueryRunner(ds);
            String sql = "SELECT * FROM ADESIVO";
            return qr.query(sql, new BeanListHandler<Adesivo>(Adesivo.class));
        } catch (SQLException ex) {
            Logger.getLogger(AdesivoRepositorio.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public Adesivo atualizar(Adesivo obj) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Adesivo deletar(Integer id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
