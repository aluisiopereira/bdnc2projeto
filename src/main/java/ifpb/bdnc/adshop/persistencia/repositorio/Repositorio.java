package ifpb.bdnc.adshop.persistencia.repositorio;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 *
 * @author José Marcondes do Nascimento Junior
 */
public interface Repositorio <T,R> extends Serializable{
    public T criar(T obj);
    public T encontrar(R id);
    public List<T> encontrar(Map<String,String> campos);
    public List<T> encontrar();
    public T atualizar(T obj);
    public T deletar(R id);
}
