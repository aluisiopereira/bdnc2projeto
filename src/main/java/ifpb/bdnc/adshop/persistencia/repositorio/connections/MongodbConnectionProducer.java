package ifpb.bdnc.adshop.persistencia.repositorio.connections;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoDatabase;
import javax.enterprise.inject.Produces;
import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;

/**
 *
 * @author José Marcondes do Nascimento Junior
 */
public class MongodbConnectionProducer {
    
    @Produces
    public MongoClient produce() throws ConfigurationException{
        Configuration configs = new PropertiesConfiguration(this.getClass().getResource("/database/dbmongo.properties"));
        return new MongoClient(configs.getString("host"), configs.getInt("port"));
    }
    
}
