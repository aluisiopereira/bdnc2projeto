package ifpb.bdnc.adshop.persistencia.repositorio;

import ifpb.bdnc.adshop.entidades.TipoUsuario;
import ifpb.bdnc.adshop.entidades.Usuario;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.inject.Inject;
import javax.sql.DataSource;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;

/**
 *
 * @author José Marcondes do Nascimento Junior
 */
public class UsuarioRepositorio implements Repositorio<Usuario, String> {

    @Inject
    private DataSource ds;

    @Override
    public Usuario criar(Usuario obj) {
        try {
            QueryRunner qr = new QueryRunner(ds);
            String sql = "INSERT INTO usuario(email, senha, nome, datanasc, sexo, tipo)"
                    + " VALUES (?, ?, ?, ?, ?, ?)";
            qr.update(sql, obj.getEmail(), obj.getSenha(), obj.getNome(), Date.valueOf(obj.getDataNascimento()),
                    obj.getSexo(), obj.getTipo().name());
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioRepositorio.class.getName()).log(Level.SEVERE, null, ex);
        }
        return obj;
    }

    @Override
    public Usuario encontrar(String id) {
        try {
            QueryRunner qr = new QueryRunner(ds);
            String sql = "SELECT * FROM USUARIO WHERE email = ?";
            return qr.query(sql, getResultSetHandler(),id);
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioRepositorio.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public List<Usuario> encontrar(Map<String, String> campos) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Usuario> encontrar() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Usuario atualizar(Usuario usr) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Usuario deletar(String id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private ResultSetHandler<Usuario> getResultSetHandler() {
        return new ResultSetHandler<Usuario>() {
            @Override
            public Usuario handle(ResultSet rs) throws SQLException {
                if (rs.next()) {
                    String email = rs.getString("email");
                    String senha = rs.getString("senha");
                    String nome = rs.getString("nome");
                    LocalDate dataNasc = rs.getDate("datanasc").toLocalDate();
                    String sexo = rs.getString("sexo");
                    TipoUsuario tipo = TipoUsuario.valueOf(rs.getString("tipo"));
                    return new Usuario(email, senha, nome, dataNasc, sexo, tipo);
                } else {
                    return null;    
                }
            }
        };
    }

}
