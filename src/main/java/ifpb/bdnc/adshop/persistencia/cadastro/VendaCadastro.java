package ifpb.bdnc.adshop.persistencia.cadastro;

import ifpb.bdnc.adshop.entidades.CarrinhoCompra;
import ifpb.bdnc.adshop.entidades.Venda;
import ifpb.bdnc.adshop.persistencia.repositorio.Repositorio;
import ifpb.bdnc.adshop.persistencia.repositorio.VendaRelacao;
import java.io.Serializable;
import javax.inject.Inject;

/**
 *
 * @author José Marcondes do Nascimento Junior
 */
public class VendaCadastro implements Serializable{
    
    @Inject
    private Repositorio<Venda, String> repositorio;
    @Inject
    private VendaRelacao vendaRelacao;
    
    public void cadastrarVenda(CarrinhoCompra cc){
        Venda venda = new Venda(cc);
        repositorio.criar(venda);
        vendaRelacao.criarRelacao(venda);
    }
        
}
