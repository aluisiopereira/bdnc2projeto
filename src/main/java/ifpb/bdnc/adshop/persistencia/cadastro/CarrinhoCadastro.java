package ifpb.bdnc.adshop.persistencia.cadastro;

import ifpb.bdnc.adshop.entidades.CarrinhoCompra;
import ifpb.bdnc.adshop.persistencia.repositorio.Repositorio;
import java.io.Serializable;
import javax.inject.Inject;

/**
 *
 * @author José Marcondes do Nascimento Junior
 */
public class CarrinhoCadastro implements Serializable{

    @Inject
    private Repositorio<CarrinhoCompra, String> repositorio;
    
    public CarrinhoCompra encontrar(String id){
        return repositorio.encontrar(id);
    }
    
    public void criarNovo(CarrinhoCompra carrinho){
        repositorio.criar(carrinho);
    }
    
    public CarrinhoCompra atualizar(CarrinhoCompra carrinho){
        return repositorio.atualizar(carrinho);
    }
    
    public void excluir(String id){
        repositorio.deletar(id);
    }
}
