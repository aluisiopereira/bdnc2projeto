package ifpb.bdnc.adshop.persistencia.cadastro;

import ifpb.bdnc.adshop.entidades.Adesivo;
import ifpb.bdnc.adshop.persistencia.repositorio.Repositorio;
import java.io.Serializable;
import java.util.List;
import javax.inject.Inject;

/**
 *
 * @author José Marcondes do Nascimento Junior
 */
public class AdesivoCadastro implements Serializable{
    
    @Inject
    private Repositorio<Adesivo, Integer> repositorio;
    
    public List<Adesivo> encontrarTodos(){
        return repositorio.encontrar();
    }
    
    public Adesivo encontrar(Integer id){
        return repositorio.encontrar(id);
    }
    
}
