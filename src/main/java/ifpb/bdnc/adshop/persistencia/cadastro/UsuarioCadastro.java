package ifpb.bdnc.adshop.persistencia.cadastro;

import ifpb.bdnc.adshop.entidades.TipoUsuario;
import ifpb.bdnc.adshop.entidades.Usuario;
import ifpb.bdnc.adshop.persistencia.repositorio.Repositorio;
import java.io.Serializable;
import java.time.LocalDate;
import javax.inject.Inject;

/**
 *
 * @author José Marcondes do Nascimento Junior
 */
public class UsuarioCadastro implements Serializable{

    @Inject
    private Repositorio<Usuario, String> repositorio;
    
    public void cadastrarUsuario(String email, String senha, String nome, LocalDate dataNascimento, String sexo, TipoUsuario tipo){
        Usuario usr = new Usuario(email, senha, nome, dataNascimento, sexo, tipo);
        repositorio.criar(usr);
    }
    
    public Usuario encontrar(String email){
        return repositorio.encontrar(email);
    }

}
