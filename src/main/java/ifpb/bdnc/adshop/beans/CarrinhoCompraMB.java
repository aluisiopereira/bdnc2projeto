package ifpb.bdnc.adshop.beans;

import ifpb.bdnc.adshop.entidades.Adesivo;
import ifpb.bdnc.adshop.entidades.CarrinhoCompra;
import ifpb.bdnc.adshop.entidades.Usuario;
import ifpb.bdnc.adshop.persistencia.cadastro.AdesivoCadastro;
import ifpb.bdnc.adshop.persistencia.cadastro.CarrinhoCadastro;
import ifpb.bdnc.adshop.persistencia.cadastro.UsuarioCadastro;
import ifpb.bdnc.adshop.seguranca.User;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.picketlink.Identity;

/**
 *
 * @author José Marcondes do Nascimento Junior
 */
@Named(value = "carrinhoCompraMB")
@RequestScoped
public class CarrinhoCompraMB {
    
    private int quant = 1;
    private int adesivoId;

    @Inject
    private CarrinhoCadastro carrinhoCadastro;
    @Inject
    private UsuarioCadastro usuarioCadastro;
    @Inject
    private Identity identity;

    /**
     * Creates a new instance of CarrinhoCompraMB
     */
    public CarrinhoCompraMB() {
    }

    public void addAdesivo(Adesivo ad) {
        FacesContext fCtx = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) fCtx.getExternalContext().getSession(true);      
        String sessionId = session.getId();
        CarrinhoCompra carrinhoCompra = carrinhoCadastro.encontrar(sessionId);
        if(carrinhoCompra == null){
            Usuario usr = usuarioCadastro.encontrar(((User) identity.getAccount()).getLoginName());
            carrinhoCompra = new CarrinhoCompra(sessionId, usr); //Recuperar usuario logado
            carrinhoCadastro.criarNovo(carrinhoCompra);
        }
        carrinhoCompra.addItem(ad, quant);
        carrinhoCadastro.atualizar(carrinhoCompra);
        System.out.println(sessionId);
        quant = 1;
    }

    public int getQuant() {
        return quant;
    }

    public void setQuant(int quant) {
        this.quant = quant;
    }

    public int getAdesivoId() {
        return adesivoId;
    }

    public void setAdesivoId(int adesivoId) {
        this.adesivoId = adesivoId;
    }

    
    
}
