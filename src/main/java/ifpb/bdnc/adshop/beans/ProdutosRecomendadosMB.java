package ifpb.bdnc.adshop.beans;

import ifpb.bdnc.adshop.entidades.Adesivo;
import ifpb.bdnc.adshop.entidades.TipoUsuario;
import ifpb.bdnc.adshop.entidades.Usuario;
import ifpb.bdnc.adshop.persistencia.cadastro.AdesivoCadastro;
import ifpb.bdnc.adshop.persistencia.cadastro.UsuarioCadastro;
import ifpb.bdnc.adshop.persistencia.repositorio.VendaRelacao;
import ifpb.bdnc.adshop.seguranca.User;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import org.picketlink.Identity;

/**
 *
 * @author José Marcondes do Nascimento Junior
 */
@Named(value = "produtosRecomendadosMB")
@ViewScoped
public class ProdutosRecomendadosMB implements Serializable{
    
    private List<Adesivo> ads;    
    private Adesivo selectedAd;
    @Inject
    private VendaRelacao vendaRelacao;
    @Inject
    private AdesivoCadastro adesivoCadastro;
    @Inject
    private UsuarioCadastro usuarioCadastro;
    @Inject
    private Identity identity;

    /**
     * Creates a new instance of ProdutosRecomendadosMB
     */
    public ProdutosRecomendadosMB() {
    }
    
    @PostConstruct
    public void init() {
        List<Adesivo> list = new ArrayList<>();
        Usuario usr = usuarioCadastro.encontrar(((User) identity.getAccount()).getLoginName());
        for(Integer id: vendaRelacao.recomendados(usr)){
            list.add(adesivoCadastro.encontrar(id));
        }
        ads = list;
    }

    public List<Adesivo> getAds() {
        return ads;
    }

    public void setAds(List<Adesivo> ads) {
        this.ads = ads;
    }

    public Adesivo getSelectedAd() {
        return selectedAd;
    }

    public void setSelectedAd(Adesivo selectedAd) {
        this.selectedAd = selectedAd;
    }
    
}
