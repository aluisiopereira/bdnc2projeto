package ifpb.bdnc.adshop.beans;

import ifpb.bdnc.adshop.entidades.TipoUsuario;
import ifpb.bdnc.adshop.seguranca.User;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.picketlink.Identity;
import org.picketlink.idm.IdentityManager;

/**
 *
 * @author José Marcondes do Nascimento Junior
 */
@Named
@RequestScoped
public class PermissoesMB {
    
    @Inject
    private Identity identity;
    
    public boolean admin(){
        User usr = (User) identity.getAccount();
        return usr.getTipo() == TipoUsuario.ADMINISTRADOR;
    }
    
}
