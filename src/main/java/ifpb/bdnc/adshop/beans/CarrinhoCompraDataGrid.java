package ifpb.bdnc.adshop.beans;

import ifpb.bdnc.adshop.entidades.Adesivo;
import ifpb.bdnc.adshop.entidades.CarrinhoCompra;
import ifpb.bdnc.adshop.entidades.ItemVenda;
import ifpb.bdnc.adshop.persistencia.cadastro.CarrinhoCadastro;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.servlet.http.HttpSession;

/**
 *
 * @author José Marcondes do Nascimento Junior
 */
@Named(value = "carrinhoCompraDataGrid")
@RequestScoped
public class CarrinhoCompraDataGrid {

    private List<Adesivo> ads;
    private Adesivo selectedAd;
    @Inject
    private CarrinhoCadastro carrinhoCadastro;

    /**
     * Creates a new instance of CarrinhoCompraDataGrid
     */
    public CarrinhoCompraDataGrid() {
    }

    @PostConstruct
    public void init() {
        FacesContext fCtx = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) fCtx.getExternalContext().getSession(true);
        String sessionId = session.getId();

        CarrinhoCompra carrinho = carrinhoCadastro.encontrar(sessionId);
        if (carrinho != null) {
            List<ItemVenda> itens = carrinho.getItens();
            List<Adesivo> list = new ArrayList<>();
            for (ItemVenda item : itens) {
                list.add(item.getItem());
            }
            ads = list;
        }else{
            ads = new ArrayList<>();
        }

    }

    public List<Adesivo> getAds() {
        return ads;
    }

    public void setAds(List<Adesivo> ads) {
        this.ads = ads;
    }

    public Adesivo getSelectedAd() {
        return selectedAd;
    }

    public void setSelectedAd(Adesivo selectedAd) {
        this.selectedAd = selectedAd;
    }
    
    

}
