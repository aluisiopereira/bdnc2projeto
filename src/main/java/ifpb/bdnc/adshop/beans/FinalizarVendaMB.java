package ifpb.bdnc.adshop.beans;

import ifpb.bdnc.adshop.entidades.CarrinhoCompra;
import ifpb.bdnc.adshop.persistencia.cadastro.CarrinhoCadastro;
import ifpb.bdnc.adshop.persistencia.cadastro.VendaCadastro;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpSession;

/**
 *
 * @author José Marcondes do Nascimento Junior
 */
@Named(value = "finalizarVendaMB")
@RequestScoped
public class FinalizarVendaMB {

    @Inject
    private CarrinhoCadastro carrinhoCadastro;
    @Inject
    private VendaCadastro vendaCadastro;

    /**
     * Creates a new instance of FinalizarVendaMB
     */
    public FinalizarVendaMB() {
    }

    public void finalizarVenda() {
        FacesContext fCtx = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) fCtx.getExternalContext().getSession(true);
        String sessionId = session.getId();
        CarrinhoCompra carrinhoCompra = carrinhoCadastro.encontrar(sessionId);
        if (carrinhoCompra != null) {
            vendaCadastro.cadastrarVenda(carrinhoCompra);
            carrinhoCadastro.excluir(sessionId);
        }
    }

}
