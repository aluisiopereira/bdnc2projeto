package ifpb.bdnc.adshop.beans;

import ifpb.bdnc.adshop.entidades.TipoUsuario;
import ifpb.bdnc.adshop.persistencia.cadastro.UsuarioCadastro;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author José Marcondes do Nascimento Junior
 */
@Named
@RequestScoped
public class CadastroUsuarioMB {
    
    private String email;
    private String senha;
    private String nome;
    private String sexo;
    private Date data = new Date();

    
    @Inject
    private UsuarioCadastro usuarioCadastro;
    
    public String cadastrar(){
        LocalDate data = this.data.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        usuarioCadastro.cadastrarUsuario(email, senha, nome, data, sexo, TipoUsuario.CLIENTE);
        
        FacesContext context = FacesContext.getCurrentInstance();        
        context.addMessage(null, new FacesMessage("Sucesso",  "Você realizou o cadastro com sucesso"));
        return "/pages/public/login.jsf";
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }
    
    
}
