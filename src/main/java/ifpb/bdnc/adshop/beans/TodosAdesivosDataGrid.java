package ifpb.bdnc.adshop.beans;

import ifpb.bdnc.adshop.entidades.Adesivo;
import ifpb.bdnc.adshop.persistencia.cadastro.AdesivoCadastro;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import javax.inject.Inject;

/**
 *
 * @author José Marcondes do Nascimento Junior
 */
@Named(value = "todosAdesivosDataGrid")
@ViewScoped
public class TodosAdesivosDataGrid implements Serializable{

    /**
     * Creates a new instance of TodosAdesivosDataGrid
     */
    public TodosAdesivosDataGrid() {
    }
    
    private List<Adesivo> ads;    
    private Adesivo selectedAd;
    @Inject
    private AdesivoCadastro adesivoCadastro;
     
     
    @PostConstruct
    public void init() {
        ads = adesivoCadastro.encontrarTodos();
    }

    public List<Adesivo> getAds() {
        return ads;
    }

    public void setAds(List<Adesivo> ads) {
        this.ads = ads;
    }

    public Adesivo getSelectedAd() {
        return selectedAd;
    }

    public void setSelectedAd(Adesivo selectedAd) {
        this.selectedAd = selectedAd;
    }
 
   
    
}
