package ifpb.bdnc.adshop.seguranca;

import javax.enterprise.event.Observes;
import org.picketlink.config.SecurityConfigurationBuilder;
import org.picketlink.event.SecurityConfigurationEvent;

/**
 *
 * @author José Marcondes do Nascimento Junior
 */
public class HttpSecurityConfiguration {

    public void onInit(@Observes SecurityConfigurationEvent event) {
        SecurityConfigurationBuilder builder = event.getBuilder();

        builder
                .http().allPaths().authenticateWith().form()
                .authenticationUri("/pages/public/login.jsf")
                .loginPage("/pages/public/login.jsf")
                .errorPage("/pages/error.jsf")
                .restoreOriginalRequest()
                .redirectTo("/pages/public/index.jsf")
                .forPath("/javax.faces.resource/*")
                .unprotected()
                .forPath("/logout")
                .logout()
                .redirectTo("/pages/public/index.jsf")
                .forPath("/pages/public/*").unprotected()
                .forPath("/index.jsf").unprotected()
                .forPath("/").unprotected()
                .forPath("/img/*").unprotected();
    }

}
