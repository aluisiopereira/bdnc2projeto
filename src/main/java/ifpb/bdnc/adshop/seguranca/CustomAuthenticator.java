package ifpb.bdnc.adshop.seguranca;

import ifpb.bdnc.adshop.entidades.Usuario;
import ifpb.bdnc.adshop.persistencia.cadastro.UsuarioCadastro;
import javax.faces.bean.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.picketlink.annotations.PicketLink;
import org.picketlink.authentication.BaseAuthenticator;
import org.picketlink.credential.DefaultLoginCredentials;

/**
 *
 * @author José Marcondes do Nascimento Junior
 */
@PicketLink
@Named
@RequestScoped
public class CustomAuthenticator extends BaseAuthenticator{
    
    @Inject 
    private DefaultLoginCredentials credentials;
    @Inject
    private UsuarioCadastro usuarioCadastro;

    public CustomAuthenticator() {
    }

    public DefaultLoginCredentials getCredentials() {
        return credentials;
    }

    public void setCredentials(DefaultLoginCredentials credentials) {
        this.credentials = credentials;
    }

    @Override
    public void authenticate() {
        String usrname = credentials.getUserId();
        String password = credentials.getPassword();
        Usuario usr = usuarioCadastro.encontrar(usrname);
        if(usr != null && usr.getSenha().equals(password)){
            setStatus(AuthenticationStatus.SUCCESS);
            setAccount(new User(usr.getTipo(), usrname));
                        
        }else{
            setStatus(AuthenticationStatus.FAILURE);
        }
    }
    
    
    
}
