package ifpb.bdnc.adshop.seguranca;

import ifpb.bdnc.adshop.entidades.TipoUsuario;

/**
 *
 * @author José Marcondes do Nascimento Junior
 */
public class User extends org.picketlink.idm.model.basic.User{
    
    private TipoUsuario tipo;

    public User() {
    }

    
    public User(TipoUsuario tipo, String loginName) {
        super(loginName);
        this.tipo = tipo;
    }

    public TipoUsuario getTipo() {
        return tipo;
    }

    public void setTipo(TipoUsuario tipo) {
        this.tipo = tipo;
    }
    
    
    
}
