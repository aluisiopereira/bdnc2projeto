package ifpb.bdnc.adshop.persistencia.repositorio;

import com.mongodb.MongoClient;
import ifpb.bdnc.adshop.entidades.Adesivo;
import ifpb.bdnc.adshop.entidades.ItemVenda;
import ifpb.bdnc.adshop.entidades.Usuario;
import ifpb.bdnc.adshop.entidades.Venda;
import ifpb.bdnc.adshop.persistencia.repositorio.connections.MongodbConnectionProducer;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import org.apache.commons.configuration.ConfigurationException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author José Marcondes do Nascimento Junior
 */
public class VendaRepositorioTest {
    
    public VendaRepositorioTest() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testCriar() throws ConfigurationException {
        System.out.println("criar");
        Usuario usr = new Usuario("email@email.com");
        Adesivo a1 = new Adesivo();
        a1.setId(1);
        Adesivo a2 = new Adesivo();
        a2.setId(2);
        ItemVenda itemVenda1 = new ItemVenda();
        itemVenda1.setItem(a1);
        itemVenda1.setQuantidade(10);
        ItemVenda itemVenda2 = new ItemVenda();
        itemVenda2.setItem(a2);
        itemVenda2.setQuantidade(20);
        List<ItemVenda> itens = new ArrayList<>();
        itens.add(itemVenda1);
        itens.add(itemVenda2);
        
        Venda venda = new Venda();
        String id = UUID.randomUUID().toString();
        venda.setId(id);
        venda.setItens(itens);
        venda.setData(LocalDate.now());
        venda.setCliente(usr);
        
        VendaRepositorio instance = new VendaRepositorio();
        instance.setMongoDatabase(new MongodbConnectionProducer().produce());
        instance.criar(venda);
        
        Venda vendaResult = instance.encontrar(id);
        System.out.println(vendaResult);
        
    }

    
    
}
