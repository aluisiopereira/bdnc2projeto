package ifpb.bdnc.adshop.persistencia.repositorio;

import ifpb.bdnc.adshop.entidades.Adesivo;
import ifpb.bdnc.adshop.entidades.CarrinhoCompra;
import ifpb.bdnc.adshop.entidades.ItemVenda;
import ifpb.bdnc.adshop.entidades.Usuario;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import redis.clients.jedis.Jedis;

/**
 *
 * @author José Marcondes do Nascimento Junior
 */
public class CarrinhoRepositorioTest {
    
    public CarrinhoRepositorioTest() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testCriarEncontrar() {
        System.out.println("criar");
        String id = UUID.randomUUID().toString();
        CarrinhoCompra obj = new CarrinhoCompra(id,new Usuario());
        
        Adesivo a1 = new Adesivo();
        a1.setId(1);
        Adesivo a2 = new Adesivo();
        a2.setId(2);
        ItemVenda itemVenda1 = new ItemVenda();
        itemVenda1.setItem(a1);
        itemVenda1.setQuantidade(10);
        ItemVenda itemVenda2 = new ItemVenda();
        itemVenda2.setItem(a2);
        itemVenda2.setQuantidade(20);
        List<ItemVenda> itens = new ArrayList<>();
        itens.add(itemVenda1);
        itens.add(itemVenda2);
        
        
        
        obj.setItens(itens);
        CarrinhoRepositorio instance = new CarrinhoRepositorio();
        instance.setJedis(new Jedis("localhost"));
        instance.criar(obj);
        CarrinhoCompra encontrado = instance.encontrar(id);
        assertEquals(2, instance.encontrar("id").getItens().size());
    }

    
}
